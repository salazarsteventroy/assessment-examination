﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssessmentExamMiniCarSales.Models
{
    public class DBCars
    {
        [Key] 
        public int Id { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string carName { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string description { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string year { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string make { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string model { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string priceData { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string daprice { get; set; }

        [Column(TypeName = "nvarchar(100)")]

        public string egcprice { get; set; }


        [Column(TypeName = "nvarchar(15)")]
        public string contactNumber { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string email { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public Boolean isFlagged { get; set; }


        [Column(TypeName = "nvarchar(100)")]

        public string abn { get; set; }
    }
}
