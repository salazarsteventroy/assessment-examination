﻿using Microsoft.EntityFrameworkCore;

namespace AssessmentExamMiniCarSales.Models
{
    public class DBCarsContext:DbContext
    {
        public DBCarsContext(DbContextOptions<DBCarsContext> options):base(options)
        {

        }

     public DbSet<DBCars> Dbcars { get; set; }
    }
}
