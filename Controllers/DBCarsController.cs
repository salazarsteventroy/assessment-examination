﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AssessmentExamMiniCarSales.Models;

namespace AssessmentExam_MiniCarsales.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DBCarsController : ControllerBase
    {
        private readonly DBCarsContext _context;

        public DBCarsController(DBCarsContext context)
        {
            _context = context;
        }

        // GET: api/DBCars
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DBCars>>> GetDbcars()
        {
          if (_context.Dbcars == null)
          {
              return NotFound();
          }
            return await _context.Dbcars.ToListAsync();
        }

        // GET: api/DBCars/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DBCars>> GetDBCars(int id)
        {
          if (_context.Dbcars == null)
          {
              return NotFound();
          }
            var dBCars = await _context.Dbcars.FindAsync(id);

            if (dBCars == null)
            {
                return NotFound();
            }

            return dBCars;
        }

        // PUT: api/DBCars/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDBCars(int id, DBCars dBCars)
        {
                      if (id != dBCars.Id)
                        {
                            return BadRequest();
                        }

            
            //dBCars.Id = id;
            
            _context.Entry(dBCars).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DBCarsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DBCars
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<DBCars>> PostDBCars(DBCars dBCars)
        {
          if (_context.Dbcars == null)
          {
              return Problem("Entity set 'DBCarsContext.Dbcars'  is null.");
          }
            _context.Dbcars.Add(dBCars);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDBCars", new { id = dBCars.Id }, dBCars);
        }

        // DELETE: api/DBCars/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDBCars(int id)
        {
            if (_context.Dbcars == null)
            {
                return NotFound();
            }
            var dBCars = await _context.Dbcars.FindAsync(id);
            if (dBCars == null)
            {
                return NotFound();
            }

            _context.Dbcars.Remove(dBCars);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool DBCarsExists(int id)
        {
            return (_context.Dbcars?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
