import React, { Component } from 'react';
import CarList from './components/carList';

export default class App extends Component {
  static displayName = App.name;

  render() {
    return (
      <div>
        <CarList/>
      </div>
    );
  }
}
