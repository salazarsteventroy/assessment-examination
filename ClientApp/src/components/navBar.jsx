import React, { useState } from 'react';
import '../css/styles.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AddCarModal from './addCarModal';

function Navbar({ searchTerm, setSearchTerm }) {
  const [showAddCarModal, setShowAddCarModal] = useState(false);

  const handleSearch = (e) => {
    e.preventDefault();
  };

  const handleAddCar = () => {
    setShowAddCarModal(true);
  };

  const handleCloseModal = () => {
    setShowAddCarModal(false);
  };

  return (
    <>
      <nav className="navbar navbar-dark bg-dark">
        <form className="d-flex m-3" onSubmit={handleSearch}>
          <input
            className="form-control me-2"
            type="text"
            placeholder="Search..."
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
          <button className="btn btn-outline-light" type="submit">
            Search
          </button>
        </form>
        <h3 className="navbar-text text-light m-3">Mini-CarSales</h3>
        <button className="btn btn-outline-light m-3" onClick={handleAddCar}>
          Add Car
        </button>
      </nav>
      {showAddCarModal && <AddCarModal handleCloseModal={handleCloseModal} />}
    </>
  );
}

export default Navbar;
