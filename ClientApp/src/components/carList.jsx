import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useNavigate, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { publicRequest } from '../requestMethods';
import Navbar from './navBar';
import EditInfoModal from './editInfoModal';
import '../css/styles.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import CarDetailsModal from './showInfoModal';

const CarList = () => {
  const location = useLocation();
  const [cars, setCars] = useState([]);
  const [filteredCars, setFilteredCars] = useState([]);
  const navigate = useNavigate();
  const [searchTerm, setSearchTerm] = useState('');
  const [editCarId, setEditCarId] = useState(null);
  const [selectedCar, setSelectedCar] = useState(null);

  useEffect(() => {
    const getCars = async () => {
      try {
        const res = await publicRequest.get('/DBCars');
        setCars(res.data);
      } catch (error) {
        console.error(error);
      }
    };

    getCars();
  }, []);

  useEffect(() => {
    let filtered = cars;

    if (searchTerm) {
      filtered = filtered.filter((item) =>
        item.carName && typeof item.carName === 'string'
          ? item.carName.toLowerCase().includes(searchTerm.toLowerCase())
          : false
      );
    }

    setFilteredCars(filtered);
  }, [cars, searchTerm]);

  const handleEditCar = (carId) => {
    setEditCarId(carId);
  };

  const handleCloseModal = () => {
    setEditCarId(null);
  };

  const handleCarClick = (carId) => {
    if (editCarId) {
      // If editCarId is set, return early to prevent showing the car details modal
      return;
    }
    setSelectedCar(carId);
  };

  const handleCarDetailsClose = () => {
    setSelectedCar(null);
  };

  const renderPrice = (car) => {
    if (car.priceData === 'DAP') {
      return car.daprice;
    } else if (car.priceData === 'EGC') {
      return car.egcprice;
    } else {
      return '';
    }
  };

  return (
    <div className="container">
      <Navbar searchTerm={searchTerm} setSearchTerm={setSearchTerm} />
      <table className="table table-dark">
        <thead>
          <tr>
            <th>Car Name</th>
            <th>Description</th>
            <th>Year</th>
            <th>Model</th>
            <th>Price</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {filteredCars.length > 0 ? (
            filteredCars.map((car) => (
              <tr
                key={car.id}
                onClick={() => handleCarClick(car.id)}
                style={{ cursor: 'pointer' }}
              >
                <td>{car.carName}</td>
                <td>{car.description}</td>
                <td>{car.year}</td>
                <td>{car.model}</td>
                <td>{renderPrice(car)}</td>
                <td>
                  <button
                    className="btn btn-primary"
                    onClick={() => handleEditCar(car.id)}
                  >
                    Edit
                  </button>
                </td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan="6">No cars found</td>
            </tr>
          )}
        </tbody>
      </table>

      {editCarId && (
        <EditInfoModal carId={editCarId} handleCloseModal={handleCloseModal} />
      )}

      {selectedCar && (
        <CarDetailsModal
          car={filteredCars.find((car) => car.id === selectedCar)}
          handleClose={handleCarDetailsClose}
        />
      )}
    </div>
  );
};

export default CarList;
