import React, { useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import { publicRequest } from '../requestMethods';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const AddCarModal = ({ handleCloseModal }) => {
  const [carName, setCarName] = useState('');
  const [description, setDescription] = useState('');
  const [year, setYear] = useState('');
  const [make, setMake] = useState('');
  const [model, setModel] = useState('');
  const [priceData, setPriceData] = useState('');
  const [daprice, setDaPrice] = useState('');
  const [egcprice, setEgcPrice] = useState('');
  const [contactNumber, setContactNumber] = useState('');
  const [email, setEmail] = useState('');
  const [isFlagged, setIsFlagged] = useState(false);
  const [abn, setAbn] = useState('');

  const handleAddCar = async () => {
    try {
      if (isFlagged && abn === '0') {
        toast.error('ABN cannot be 0 when the car is flagged');
        return;
      }

      await publicRequest.post('/DBCars', {
        carName,
        description,
        year,
        make,
        model,
        priceData,
        daprice,
        egcprice,
        contactNumber,
        email,
        isFlagged,
        abn,
      });

      toast.success('Car added successfully');
      handleCloseModal();
    } catch (error) {
      console.error('Error adding car', error);
      toast.error('Error adding car');
    }
  };

  return (
    <Modal show onHide={handleCloseModal}>
      <Modal.Header closeButton>
        <Modal.Title>Add Car</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group controlId="carName">
            <Form.Label>Car Name</Form.Label>
            <Form.Control
              type="text"
              value={carName}
              onChange={(e) => setCarName(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="description">
            <Form.Label>Description</Form.Label>
            <Form.Control
              type="text"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="year">
            <Form.Label>Year</Form.Label>
            <Form.Control
              type="text"
              value={year}
              onChange={(e) => setYear(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="make">
            <Form.Label>Make</Form.Label>
            <Form.Control
              type="text"
              value={make}
              onChange={(e) => setMake(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="model">
            <Form.Label>Model</Form.Label>
            <Form.Control
              type="text"
              value={model}
              onChange={(e) => setModel(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="priceData">
            <Form.Label>Price Data</Form.Label>
            <Form.Control
              type="text"
              value={priceData}
              onChange={(e) => setPriceData(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="daprice">
            <Form.Label>DA Price</Form.Label>
            <Form.Control
              type="text"
              value={daprice}
              onChange={(e) => setDaPrice(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="egcprice">
            <Form.Label>EGC Price</Form.Label>
            <Form.Control
              type="text"
              value={egcprice}
              onChange={(e) => setEgcPrice(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="contactNumber">
            <Form.Label>Contact Number</Form.Label>
            <Form.Control
              type="text"
              value={contactNumber}
              onChange={(e) => setContactNumber(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="email">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="isFlagged">
            <Form.Check
              type="checkbox"
              label="Is Flagged"
              checked={isFlagged}
              onChange={(e) => setIsFlagged(e.target.checked)}
            />
          </Form.Group>
          <Form.Group controlId="abn">
            <Form.Label>ABN</Form.Label>
            <Form.Control
              type="text"
              value={abn}
              onChange={(e) => setAbn(e.target.value)}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleCloseModal}>
          Close
        </Button>
        <Button variant="primary" onClick={handleAddCar}>
          Add Car
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default AddCarModal;
