import React from 'react';
import { Modal, Button } from 'react-bootstrap';

const CarDetailsModal = ({ car, handleClose }) => {
  return (
    <Modal show={true} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Car Details</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>Car Name: {car.carName}</p>
        <p>Description: {car.description}</p>
        <p>Year: {car.year}</p>
        <p>Make: {car.make}</p>
        <p>Model: {car.model}</p>
        <p>Price Data: {car.priceData}</p>
        <p>DA Price: {car.daprice}</p>
        <p>EGC Price: {car.egcprice}</p>
        <p>Contact Number: {car.contactNumber}</p>
        <p>Email: {car.email}</p>
        <p>Is Flagged: {car.isFlagged ? 'Yes' : 'No'}</p>
        <p>ABN: {car.abn}</p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default CarDetailsModal;
