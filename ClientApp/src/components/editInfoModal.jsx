import React, { useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { publicRequest } from '../requestMethods';

const EditInfoModal = ({ carId, handleCloseModal }) => {
  const [carName, setCarName] = useState('');
  const [carDescription, setCarDescription] = useState('');
  const [carYear, setCarYear] = useState('');
  const [carMake, setCarMake] = useState('');
  const [carModel, setCarModel] = useState('');
  const [priceData, setPriceData] = useState('');
  const [daPrice, setDaPrice] = useState(0);
  const [egcPrice, setEgcPrice] = useState(0);
  const [contactNumber, setContactNumber] = useState('');
  const [email, setEmail] = useState('');
  const [isFlagged, setIsFlagged] = useState(false);
  const [selectedCar, setSelectedCar] = useState(null);
  const [abn, setAbn] = useState('');
  const [abnError, setAbnError] = useState('');


  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await publicRequest.get(`/DBCars/${carId}`);
        const carData = response.data;

        setCarName(carData.carName);
        setCarDescription(carData.description);
        setCarYear(carData.year);
        setCarMake(carData.make);
        setCarModel(carData.model);
        setPriceData(carData.priceData);
        setDaPrice(carData.daPrice);
        setEgcPrice(carData.egcPrice);
        setContactNumber(carData.contactNumber);
        setEmail(carData.email);
        setIsFlagged(carData.isFlagged);
      } catch (error) {
        console.error('Error fetching car data', error);
      }
    };

    fetchData();
  }, [carId]);

  const handleUpdate = async () => {
    if (isFlagged && abn === '0') {
        setAbnError('ABN cannot be 0 when flagged');
        return;
      }
    try {
      await publicRequest.put(`/DBCars/${carId}`, {
        carName,
        description: carDescription,
        year: carYear,
        make: carMake,
        model: carModel,
        priceData,
        daPrice,
        egcPrice,
        contactNumber,
        email,
        isFlagged
      });

      console.log('Updated Successfully');
    } catch (error) {
      console.error('Error updating info', error);
    }
  };

  const handleDelete = async () => {
    try {
      await publicRequest.delete(`/DBCars/${carId}`);
      console.log('Car deleted successfully');
      handleCloseModal();
    } catch (error) {
      console.error('Error deleting car', error);
    }
  };

  const handleEditClick = () => {
    setSelectedCar(carId);
  };


  return (
    <>
      <div className="modal-body">
        <Form.Group controlId="formCarName">
          <Form.Label>Car Name</Form.Label>
          <Form.Control
            type="text"
            value={carName}
            onChange={(e) => setCarName(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="formCarDescription">
          <Form.Label>Car Description</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            value={carDescription}
            onChange={(e) => setCarDescription(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="formCarYear">
          <Form.Label>Car Year</Form.Label>
          <Form.Control
            type="text"
            value={carYear}
            onChange={(e) => setCarYear(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="formCarMake">
          <Form.Label>Car Make</Form.Label>
          <Form.Control
            type="text"
            value={carMake}
            onChange={(e) => setCarMake(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="formCarModel">
          <Form.Label>Car Model</Form.Label>
          <Form.Control
            type="text"
            value={carModel}
            onChange={(e) => setCarModel(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="formPriceData">
          <Form.Label>Price Data</Form.Label>
          <Form.Control
            type="text"
            value={priceData}
            onChange={(e) => setPriceData(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="formDaPrice">
          <Form.Label>DA Price</Form.Label>
          <Form.Control
            type="test"
            value={daPrice}
            onChange={(e) => setDaPrice(Number(e.target.value))}
          />
        </Form.Group>

        <Form.Group controlId="formEgcPrice">
          <Form.Label>EGC Price</Form.Label>
          <Form.Control
            type="text"
            value={egcPrice}
            onChange={(e) => setEgcPrice(Number(e.target.value))}
          />
        </Form.Group>

        <Form.Group controlId="formContactNumber">
          <Form.Label>Contact Number</Form.Label>
          <Form.Control
            type="text"
            value={contactNumber}
            onChange={(e) => setContactNumber(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="formEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="formIsFlagged">
          <Form.Check
            type="checkbox"
            label="Is Flagged"
            checked={isFlagged}
            onChange={(e) => setIsFlagged(e.target.checked)}
          />
        </Form.Group>
      </div>
      <div className="modal-footer m-3 p-3">
  <Button style={{ marginRight: '10px' }} variant="primary" onClick={handleUpdate}>
    Update
  </Button>
  <Button style={{ marginRight: '10px' }} variant="danger" onClick={handleDelete}>
    Delete
  </Button>
  <Button variant="secondary" onClick={handleCloseModal}>
    Close
  </Button>
</div>

    </>
  );
};

export default EditInfoModal;
