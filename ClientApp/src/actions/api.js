import axios from "axios"

const baseUrl = "http://localhost:5211/api"

export default {
    dCar(url){
        return {
            fetchAll: () => axios.get(url)
        }
    }
}