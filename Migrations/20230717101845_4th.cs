﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AssessmentExam_MiniCarsales.Migrations
{
    /// <inheritdoc />
    public partial class _4th : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EGCprice",
                table: "Dbcars",
                newName: "egcprice");

            migrationBuilder.RenameColumn(
                name: "DAprice",
                table: "Dbcars",
                newName: "daprice");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "egcprice",
                table: "Dbcars",
                newName: "EGCprice");

            migrationBuilder.RenameColumn(
                name: "daprice",
                table: "Dbcars",
                newName: "DAprice");
        }
    }
}
