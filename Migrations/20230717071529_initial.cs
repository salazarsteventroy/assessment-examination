﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AssessmentExam_MiniCarsales.Migrations
{
    /// <inheritdoc />
    public partial class initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "price",
                table: "Dbcars",
                newName: "EGCprice");

            migrationBuilder.AddColumn<string>(
                name: "DAprice",
                table: "Dbcars",
                type: "nvarchar(100)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DAprice",
                table: "Dbcars");

            migrationBuilder.RenameColumn(
                name: "EGCprice",
                table: "Dbcars",
                newName: "price");
        }
    }
}
