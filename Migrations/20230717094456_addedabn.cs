﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AssessmentExam_MiniCarsales.Migrations
{
    /// <inheritdoc />
    public partial class addedabn : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ABN",
                table: "Dbcars",
                type: "nvarchar(100)",
                nullable: true,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ABN",
                table: "Dbcars");
        }
    }
}
