﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AssessmentExam_MiniCarsales.Migrations
{
    /// <inheritdoc />
    public partial class InititialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Dbcars",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    carName = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    description = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    year = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    make = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    model = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    priceData = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    price = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    contactNumber = table.Column<string>(type: "nvarchar(15)", nullable: false),
                    email = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    isFlagged = table.Column<string>(type: "nvarchar(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dbcars", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Dbcars");
        }
    }
}
