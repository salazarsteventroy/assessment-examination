﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AssessmentExam_MiniCarsales.Migrations
{
    /// <inheritdoc />
    public partial class _3rd : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ABN",
                table: "Dbcars",
                newName: "abn");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "abn",
                table: "Dbcars",
                newName: "ABN");
        }
    }
}
